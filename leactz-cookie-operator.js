// SEARCH THROUGH ALL THE COOKIES
// SEPARATE COOKIES AND STORE THEM IN AN ARRAY
// SEPARATE COOKIE ARRAY ITEMS INTO COOKIE NAME AND COOKIE VALUE
// CHECK IF A SPECIFIC COOKIE EXISTS
// CHECK THE VALUE OF THAT SPECIFIC COOKIE
// DO SOMETHING BASED ON THAT VALUE
// NO REGEXP NEEDED


// initializes a placeholder variable for cookie value returned by cookie function (global scope)
let cookieValuePlaceholder;

function cookieFunction() {
  // Creates an array from all cookies
  const cookieArray = document.cookie.split(';');

  // Loops through cookie array
  cookieArray.forEach((cookie, i) => {
    // trims whitespace from cookie if whitespace exists
    const cTrim = cookie.trim();
    // separates the name and the value of the cookie, stores name and value in an array [name, value]
    const cookieContent = cTrim.split('=');
    const cookieName = cookieContent[0];
    const cookieVal = cookieContent[1];
    // logs cookie name and value using a string literal
    console.log(`COOKIE ARRAY POSITION ${i} : The cookie name is "${cookieName}" and the value is "${cookieVal}"`);
    // logic to check if a cookie exists with a certain value
    if( (cookieName === 'disco_time') && (cookieVal === '3_AM')){
      // assigns a value to the placeholder variable that will be returned to the global scope
      cookieValuePlaceholder = `According to the cookie "${cookieName}", Moose is very late arriving home from the disco.`;

      // returns the value of the placeholder variable to the scope of the conditional loop
      return cookieValuePlaceholder;
    }
    // returns value of the plaeholder variable to the main cookie function and global scope
    return cookieValuePlaceholder
  });
  }


// Sets a cookie and tests the function
// requires an element with the class of 'set-cookie' to be clicked
// this will set the test cookies for the duration of the session
const setCookie = document.querySelector('.set-cookie');

setCookie.addEventListener('click', ()=> {
  document.cookie = "cookieOne=value_1";
  document.cookie = "the_matrix=is_almost_real";
  document.cookie = "disco_time=3_AM";
  document.cookie = "banana_farm=many_bananas";
  cookieFunction();
  console.log(`THE COOKIE OPERATION SAYS: '${cookieValuePlaceholder}'`);
});

